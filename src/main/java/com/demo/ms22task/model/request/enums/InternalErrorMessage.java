package com.demo.ms22task.model.request.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum InternalErrorMessage {

    INTERNAL_SERVER_ERROR("Technocal error occurred");

    private final String message;

}
