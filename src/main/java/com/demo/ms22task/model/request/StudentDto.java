package com.demo.ms22task.model.request;

import com.demo.ms22task.validation.group.CreateAction;
import com.demo.ms22task.validation.StudentInfo;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;


import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldNameConstants
@StudentInfo(groups = CreateAction.class)
public class StudentDto {


    @NotBlank(message = "Student number must not be blank")
    private String studentNumber;
    @NotBlank(message = "Student name must not be blank")
    private String name;
    @NotBlank(message = "Student surname must not be blank")
    private String surname;
    @NotNull(message = "Student group number must not be null")
    private String groupNumber;
    @Min(value = 1,message = "scholarship must be between 1 - 1000")
    private BigDecimal scholarship;
    private String major;

}
