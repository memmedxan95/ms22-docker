package com.demo.ms22task.dao.repository;

import com.demo.ms22task.dao.entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity,Long>, JpaSpecificationExecutor<StudentEntity> {


    @Query("select s from StudentEntity s where (:name is null or s.name=:name) " +
            "or (:surname is null or s.surname=:surname) " +
            "or (:studentNumber is null or s.studentNumber=:studentNumber) " +
            "or (:groupNumber is null or s.groupNumber=:groupNumber) " +
            "or (:major is null or s.major=:major)")
    List<StudentEntity> findStudentsByParameters(@Param("name") String name,
                                                 @Param("surname") String surname,
                                                 @Param("studentNumber") String studentNumber,
                                                 @Param("groupNumber") String groupNumber,
                                                 @Param("major") String major);
}
