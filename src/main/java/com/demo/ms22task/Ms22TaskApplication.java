package com.demo.ms22task;

import com.demo.ms22task.dao.repository.StudentRepository;
import com.demo.ms22task.model.request.StudentDto;
import com.demo.ms22task.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms22TaskApplication implements CommandLineRunner {


    private final StudentService studentService;
    private final StudentRepository studentRepository;

    public static void main(String[] args) {
        SpringApplication.run(Ms22TaskApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        List<StudentDto> allStudents = studentService.getStudents("Memmedxan","Bag","256+56","6656", "baytar");
//        allStudents.forEach(System.out::println);

//        System.out.println(studentService.getStudentById());

    }
}
