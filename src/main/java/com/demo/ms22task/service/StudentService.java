package com.demo.ms22task.service;

import com.demo.ms22task.dao.entity.StudentEntity;
import com.demo.ms22task.dao.repository.StudentRepository;
import com.demo.ms22task.exception.StudentNotFoundException;
import com.demo.ms22task.model.request.SearchCriteria;
import com.demo.ms22task.model.request.StudentDto;
import com.demo.ms22task.model.request.enums.ErrorMessage;
import com.demo.ms22task.specification.StudentSpecification;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.demo.ms22task.mapper.StudentMapper.STUDENT_MAPPER;
import static java.util.Arrays.stream;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;



    public void createStudent(StudentDto studentDto){
        var mappedStudent = STUDENT_MAPPER.mapToStudentEntity(studentDto);
        studentRepository.save(mappedStudent);
    }

    public StudentDto getStudentById(Long id){
        return studentRepository.findById(id)
                .map(STUDENT_MAPPER::mapToStudentDto)
                .orElseThrow(()-> new StudentNotFoundException(ErrorMessage.STUDENT_NOT_FOUND.getMessage(id)));
    }


    public List<StudentDto> getAllStudents1(){
        return studentRepository.findAll()
                .stream()
                .map(STUDENT_MAPPER::mapToStudentDto)
                .toList();
    }

    public void deleteStudent(Long id){

        studentRepository.deleteById(id);


    }

    public List<StudentDto> getAllStudents2(String name, String surname){

        Specification<StudentEntity> studentDtoSpecification = null;
        Specification<StudentEntity> studentDtoSpecification3 = null;

        List<Predicate> predicateList = new ArrayList<>();

        if (name!= null){
            studentDtoSpecification = (root, cq, cb)-> cb.equal(root.get("name"),name);
        }




        studentDtoSpecification3 = (root , cq, cb) -> {

            if(name != null){
                predicateList.add(cb.equal(root.get("name"),name));
            }
            if (surname!=null){
                predicateList.add(cb.equal(root.get("surname"),surname));
            }
                cq.where(cb.or(predicateList.toArray(new Predicate[0])));
                return cq.getRestriction();
        };

        Specification<StudentEntity> studentDtoSpecification2 = new Specification<StudentEntity>() {
            @Override
            public Predicate toPredicate(Root<StudentEntity> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.like(root.get("name"),"%" + name + "%");
            }
        };

          return studentRepository.findAll(studentDtoSpecification3)
                  .stream()
                  .map(STUDENT_MAPPER::mapToStudentDto)
                  .toList();



    }

      public List<StudentDto> getStudents(String name, String surname, String studentNumber, String groupNumber, String major){
              return  studentRepository.findStudentsByParameters(name,surname,studentNumber,groupNumber,major)
                        .stream()
                        .map(STUDENT_MAPPER::mapToStudentDto)
                        .toList();
            }

    public List<StudentEntity> findAllStudents(List<SearchCriteria> searchCriteriaList) {
        StudentSpecification studentSpecification = new StudentSpecification();
        searchCriteriaList.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
        return studentRepository.findAll(studentSpecification);

    }

    public Page<StudentEntity> getStudentAll(int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).descending());
        return studentRepository.findAll(pageable);
    }




}
