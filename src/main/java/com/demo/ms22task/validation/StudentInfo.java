package com.demo.ms22task.validation;

import com.demo.ms22task.validation.impl.StudentInfoValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint(validatedBy = StudentInfoValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface StudentInfo {

    String message() default "Firstname and lastname should be filled in ";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
