package com.demo.ms22task.validation.impl;

import com.demo.ms22task.dao.repository.StudentRepository;
import com.demo.ms22task.model.request.StudentDto;
import com.demo.ms22task.validation.StudentInfo;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.hasText;


@Component
@RequiredArgsConstructor
public class StudentInfoValidator implements ConstraintValidator<StudentInfo, StudentDto> {

    private final StudentRepository studentRepository;

    @Override
    public boolean isValid(StudentDto value, ConstraintValidatorContext context) {
        return hasText(value.getName()) ||  hasText(value.getSurname());
    }
}
