package com.demo.ms22task.controller;


import com.demo.ms22task.dao.entity.StudentEntity;
import com.demo.ms22task.model.request.SearchCriteria;
import com.demo.ms22task.model.request.StudentDto;
import com.demo.ms22task.service.StudentService;
import com.demo.ms22task.validation.group.CreateAction;
import jakarta.validation.groups.Default;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("v1/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;


    @PostMapping()
    @ResponseStatus(CREATED)
    public void createStudent(@RequestBody @Validated({Default.class, CreateAction.class}) StudentDto studentDto){
        studentService.createStudent(studentDto);
    }

//    @PostMapping()
//    @ResponseStatus(CREATED)
//    public String update(@PathVariable("id") Long id, @Validated({Default.class, UpdateAction.class}) StudentDto studentDto){
//            return "tamamla sonra";
//    }

    @GetMapping("/get1")
    public List<StudentDto> getAllStudents1(){
      return  studentService.getAllStudents1();
    }

    @GetMapping("/{id}")
    public StudentDto getById(@PathVariable Long id){
     return   studentService.getStudentById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id){
        studentService.deleteStudent(id);
    }


    @GetMapping("/get2/{name}   ")
    public List<StudentDto> getAllStudents2(String name, String surname){
        return  studentService.getAllStudents2(name, surname);
    }

    @PostMapping("/student")
    public ResponseEntity<List<StudentEntity>> findAllStudents(@RequestBody List<SearchCriteria> searchCriteriaList) {
        return ResponseEntity.ok(studentService.findAllStudents(searchCriteriaList));
    }

    @GetMapping("/student/all")
    public ResponseEntity<Page<StudentEntity>> getStudentAll(@RequestParam(value = "pageSize") int pageSize,
                                                             @RequestParam(value = "pageNumber") int pageNumber,
                                                             @RequestParam(value = "pageSort") String[] pageSort) {
        return ResponseEntity.ok(studentService.getStudentAll(pageSize, pageNumber, pageSort));
    }






}
