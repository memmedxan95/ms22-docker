package com.demo.ms22task.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ErrorResponse {
    private LocalDate timestamp;
    private String message;
    private Integer statusCode;
    private String path;
    private List<String> validationsMessages;
//    private String severity;
//    private String info;

}
