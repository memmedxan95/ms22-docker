package com.demo.ms22task.exception;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.time.LocalDate;
import java.util.ArrayList;

import static com.demo.ms22task.model.request.enums.InternalErrorMessage.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(StudentNotFoundException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleStudentNotFound(HttpServletRequest request,
                                               StudentNotFoundException exception
            , HttpServletResponse response) {
        return ErrorResponse.builder()
                .timestamp(LocalDate.now())
                .message(exception.getMessage())
                .statusCode(BAD_REQUEST.value())
                .path(request.getServletPath())
                .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleInternalServerError(HttpServletRequest request) {
        return ErrorResponse.builder()
                .timestamp(LocalDate.now())
                .message(INTERNAL_SERVER_ERROR.getMessage())
                .statusCode(BAD_REQUEST.value())
                .path(request.getServletPath())
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleMethodArgumentNot(HttpServletRequest request,
                                                 MethodArgumentNotValidException exception,
                                                 HttpServletResponse response) {
        ArrayList <String> errors = new ArrayList<>();
        exception.getBindingResult()
                .getFieldErrors()
               .stream()
               .forEach(fieldError -> errors.add(fieldError.getDefaultMessage()));

        return ErrorResponse.builder()
                .timestamp(LocalDate.now())
                .validationsMessages(errors)
                .statusCode(BAD_REQUEST.value())
                .path(request.getServletPath())
                .build();

    }



//    @ExceptionHandler(StudentNotFoundException.class)
//    @ResponseStatus(BAD_REQUEST)
//    public ResponseEntity<Map<String,String>> handle(HttpServletRequest request,
//                                                     StudentNotFoundException exception,
//                                                     HttpServletResponse response) {
//        Map<String,String> error= new HashMap<>();
//        error.put("timestamp", LocalDate.now().toString());
//        error.put("status",BAD_REQUEST.value());
//        error.put("message",exception.getMessage());
//        error.put("path",request.getServletPath());
//        return ResponseEntity.status((int)(response.getStatus()).body(error);
//    }


    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(BAD_REQUEST)
    public ErrorResponse handleHttpMessageNotReadable(HttpServletRequest request,
                                                      MethodArgumentNotValidException exception,
                                                      HttpServletResponse response) {
        return ErrorResponse.builder()
                .timestamp(LocalDate.now())
                .message(exception.getFieldError()
                        .getDefaultMessage())
                .statusCode(BAD_REQUEST.value())
                .path(request.getServletPath())
                .build();
    }
}